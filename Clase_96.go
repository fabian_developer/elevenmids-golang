package main

import (
	"fmt"
)

type persona struct {
	nombre   string
	apellido string
}

type agenteSecreto struct {
	persona
	lpm bool
}

func (s agenteSecreto) presentar() {
	fmt.Println("Hola, soy", s.nombre, s.apellido, "- el agente secreto se presenta.")
}

func (p persona) presentar() {
	fmt.Println("Hola, soy", p.nombre, p.apellido, "- la persona se presenta.")
}

type humano interface {
	presentar()
}

func bar(h humano) {
	fmt.Println("Fui pasado a la función bar", h)
}

func main() {
	as1 := agenteSecreto{
		persona: persona{
			"Eduar",
			"Tua",
		},
		lpm: true,
	}
	as2 := agenteSecreto{
		persona: persona{
			"Miss",
			"Moneypenny",
		},
		lpm: true,
	}
	
	p := persona {
		nombre: "Carito",
		apellido: "Guz",
	}
	
	fmt.Println(as1)
	as1.presentar()
	as2.presentar()
	bar(as1)
	bar(as2)
	bar(p)
}