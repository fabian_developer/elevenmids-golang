// recursividad
package main

import (
	"fmt"
)

func main() {
	// fmt.Print(4 * 3 * 2 * 1)
	n1 := factorial(4)
	fmt.Println(n1)

	n2 := cicloFactorial(4)
	fmt.Println(n2)
}

func factorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorial(n-1)
}

func cicloFactorial(x int) int {
	total := 1
	for ; x > 0; x-- {
		total *= x
	}
	return total
}
