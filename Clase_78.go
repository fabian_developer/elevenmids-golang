package main

import (
	"fmt"
)

func main() {
	arreglo1 := []string{"Fabian", "Jorge", "Marcos", "Ricarod"}
	arreglo2 := []string{"TI", "Conta", "LIC", "C"}

	resultado := [][]string{arreglo1, arreglo2}
	fmt.Sprintln(resultado)

	for i, reg := range resultado {
		fmt.Println("registro:", i)
		for j, val := range reg {
			fmt.Printf("\tindice de posicion: %v\tvalor: %v\n", j, val)
		}
	}
}
