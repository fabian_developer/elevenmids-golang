package main

import (
	"encoding/json"
	"fmt"
)

type persona struct {
	Nombre   string
	Apellido string
	Edad     int
}

func main() {
	p1 := persona{
		Nombre:   "Fabian",
		Apellido: "Santiago",
		Edad:     21,
	}
	p2 := persona{
		Nombre:   "Jimena",
		Apellido: "Santiago",
		Edad:     16,
	}
	personas := []persona{p1, p2}
	// fmt.Println(personas)
	bs, err := json.Marshal(personas)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(bs))
}
