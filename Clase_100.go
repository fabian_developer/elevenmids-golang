package main

import (
	"fmt"
)

func main() {
	// s1 := foo()
	// fmt.Println(s1)

	// s2 := bar()
	// fmt.Println(s2)

	// i := s2()
	fmt.Println(bar()())
}

// func foo() string {
// 	return "Hola mundo"
// }

func bar() func() int {
	return func() int {
		return 1492
	}
}
